# Lyric Timer
Adds timing to song lyrics when given the song and the lyrics using [OpenAI Whisper](https://github.com/openai/whisper).

This program is still experimental.

## Demo

A demo can be found on YouTube:

[![Screenshot of a YouTube video saying "Lyric Timer 0.1 Demo" and "Turn on captions"](img/demo.png)](https://www.youtube.com/watch?v=ZtMiCKShvBs)

Turn on closed captions. The English (United States) captions demonstrate the default mode, while the English (United Kingdom) ones demonstrate word highlighting.

## Installation
It is strongly recommended to use a virtualenv and [run Whisper on a GPU](https://github.com/openai/whisper/discussions/1173#discussioncomment-5586041).

The package can be installed using pip:
```
pip install git+https://gitlab.com/narektor/lyric-timer.git
```

To update it to the latest version, run:
```
pip install --upgrade --no-deps --force-reinstall git+https://gitlab.com/narektor/lyric-timer.git
```

## Usage
After that, call the program, providing the music and lyrics file.
```
lyrictimer path/to/song.mp3 path/to/lyrics.txt
```

By default, the lyrics with timecodes will be saved as a SubRip (srt) file with the same name as the lyrics file in the current folder. However, the program supports WebVTT (vtt, useful for YouTube subtitles) and LRC files.

### Examples
- Transcribe `another_tomorrow.mp3` with the lyrics being in `lyrics/another_tomorrow.txt`:
```
lyrictimer another_tomorrow.mp3 lyrics/another_tomorrow.txt
```
> The transcribed lyrics are in `another_tomorrow.srt` in the folder Lyric Timer was launched in.

Lyric Timer can also be configured using arguments. The entire list can be seen using `python3 -m lyrictimer -h`. Here are a few examples:

- Transcribe `Music/Favorites/Saint.mp3` to `Music/lyrics/saint.srt` with the lyrics being in `Music/lyrics/saint.txt`, using the `large-v2` model:
```
lyrictimer --model large-v2 -o Music/lyrics/saint.srt Music/Favorites/Saint.mp3 Music/lyrics/saint.txt
```

- Transcribe `Music/Favorites/Lovebirds/I3U.flac` with the lyrics being in `Music/lyrics/i_three_u.txt`, highlighting the lyrics:
```
lyrictimer -w Music/Favorites/Lovebirds/I3U.flac Music/lyrics/i_three_u.txt
```

The lyrics will be italic and underlined:

![Screenshot of a music video with the words "Here come old flat top, he come grooving up slowly". The word "flat" is highlighted.](img/highlight_mode.png)

> If you're exporting in the LRC format, the output file will be in [enhanced/A2 LRC format](https://en.wikipedia.org/wiki/LRC_(file_format)#A2_extension:_word_time_tag) with word-level timestamps. Keep in mind that not all media players support this format.

- Transcribe `Music/Favorites/Groupe_Musique/Mon_Ami.wav` with the lyrics being in `Music/lyrics/mon_ami.txt`, specifying the language as French and using the `base` model:
```
lyrictimer -l fr --model base Music/Favorites/Groupe_Musique/Mon_Ami.wav Music/lyrics/mon_ami.txt
```

## F. A. Q.

### How does this work?
The program is just using Whisper to transcribe the song, giving the lyrics as a prompt (so the AI makes less transcription errors).

### The transcribed lyrics are wrong/nonsense
Of course, no voice to text model is 100% accurate, but here are some tips to help improve the accuracy:

- Whisper's [transcription quality](https://github.com/openai/whisper#available-models-and-languages) naturally plays a huge role in the quality of the lyrics.
	- The transcription quality of songs with verses in multiple languages (e.g. the chorus or the last verse being in a different language to the rest of the song) is impacted by Whisper's accuracy in _both_ languages, regardless of the language of an individual verse.
		- For example, if a mostly English song has a verse in Russian, the quality of the timed lyrics in both the Russian verse and other parts is impacted by Whisper's accuracy in both English and Russian.
		- To mitigate this, try removing any text in lower quality languages in the lyric file. 
- Try running on a GPU, if you aren't already.
- If possible, use a different model: `lyrictimer --model <MODEL> ...`. See [this list](https://github.com/openai/whisper#available-models-and-languages). The default is `small`.
- If the language of the song is known (and especially if the transcribed lyrics contain other alphabets), try specifying the language: `lyrictimer -l <LANGUAGE> ...`.
	- Additionally, if the song is in English, try an English only model (see the tip above).
- The model works worse with heavily processed voices.
- Try phoneme-based correction. It essentially works by using the transcribed text as a guide to pick the real lyrics instead of using the transcribed lyrics.
- Try the "split vocals" feature. This sometimes results in reduced accuracy, but could help in other times.
- Sometimes, using another source for the lyrics helps.
	- Lyric files that have punctuation and don't shorten the text (e.g. using "(x2)" to indicate that the verse is repeated twice or using "(chorus)" after writing the chorus lyrics once instead of repeating the chorus every time) usually result in transcriptions that are more accurate to the source.

### A `RuntimeError: espeak not installed on your system` error appears when using phoneme-based correction
Install [espeak-ng](https://github.com/espeak-ng/espeak-ng).

If you already have it and are using Windows, you need to create an environment variable:
```
set "PHONEMIZER_ESPEAK_LIBRARY=C:\Program Files\eSpeak NG\libespeak-ng.dll"
set "PHONEMIZER_ESPEAK_PATH=C:\Program Files\eSpeak NG"
```
Assuming eSpeak is installed in the default path, `C:\Program Files\eSpeak NG`. If it's not installed there change the path.

## Usage from code
Lyric Timer can also be used as a library. Here's an example:
```python
from lyrictimer import transcribe

song_path = "path_to_a_song.wav"
lyrics_path = "path_to_lyrics.txt"
lyrics = ""
with open(lyrics_path, "r") as f:
    lyrics = f.read()

timed_lyrics = transcribe(lyrics, song_path)
```

The output is a list of objects with the transcribed data. The objects' keys are:
- `text`: the lyric
- `from`: the time the lyric starts at (seconds)
- `to`: the time the lyric ends at (seconds)

All arguments available in the CLI are also available here.

### Extracting words

If you like, you can also get the timestamps of individual words:
```python
timed_lyrics = transcribe(lyrics, song_path, split_words=True)
```

This will return a similar output (i.e. you will still get the sentences), but you will also get the words in a separate `words` key. The key is a list of objects with a similar structure to sentences:
- `word`: the word
- `from`: the time the word starts at (seconds)
- `to`: the time the word ends at (seconds)

The words' timestamps are relative to the entire song, not the sentence they're in.

### Phoneme-based correction

This is a feature that looks at the phonemes in sentences to try and correct sentences that might be incorrectly transcribed, as well as tries to fix cases where several lyrics are transcribed as one. It currently doesn't support word highlighting.

To use it from the command line, use the `--correct` argument.

To use it in code, use `correct_result` in `lyrictimer.phoneme_correction`. This function takes in a `result` from the `transcribe` function, the real lyrics and an optional verbosity parameter. It returns a `result` just like the one returned by `transcribe`, with the corrected lyrics.

**The `transcribe` function call must be done with word extraction.** While phoneme-based correction and word highlighting are incompatible, it still uses word extraction to determine when a lyric ends by looking at the words.

Example:
```python
from lyrictimer import transcribe
from lyrictimer.phoneme_correction import correct_result as ph_correct

song_path = "path_to_a_song.wav"
lyrics_path = "path_to_lyrics.txt"
lyrics = ""
with open(lyrics_path, "r") as f:
    lyrics = f.read()

timed_lyrics = transcribe(lyrics, song_path, split_words=True)
fixed_lyrics = ph_correct(timed_lyrics, lyrics)
```

### Raw exports

This is an export format that might help developers or advanced users, and is especially useful if the program fails to recognize something. It uses a special `.ltr` (**L**yric **T**imer **r**aw) extension, and can be selected like any other format (`lyrictimer -f ltr`).

Raw files are JSON files with the following structure:

* `ver` - the version.
* `params` - the configuration of the program:
	* `pbc` - is phoneme-based correction enabled?
	* `highlight` - is word highlighting enabled?
	* `adv` - advanced parameters.
	* `model` - the model used to transcribe.
	* `lang` - if specified, the language of the song.
* `lyrics` - the real lyrics of the song as an array of lines.
* `result` - the transcription output.
	* If phoneme-based correction is on, it's an object with the following keys (the values are `result` objects):
		* `raw` - the raw result as returned by the neural network.
		* `phc` - the corrected result.
	* If it's off, it's a serialized `result` object.

Eventually it will be possible to use these raw files to process lyrics again and export them to a different format without having to re-run speech recognition.
