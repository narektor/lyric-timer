def make_word_segments(lt):
    new_segments = []
    # for each word in each segment:
    for segment in lt:
        for word in segment["words"]:
            w = word["word"]
            # make a new segment with the word in italic
            lyric_new = segment["text"].replace(w, f"<u><i>{w}</i></u>")
            # add the segment
            new_segments.append({
                "text": lyric_new,
                "from": word['from'],
                "to": word['to']
            })
    return new_segments
