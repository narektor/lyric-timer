from spleeter.separator import Separator
import tempfile
import pathlib

def split_vocals(in_file, model = "spleeter:2stems"):
    separator = Separator(model)
    out_dir = tempfile.TemporaryDirectory(prefix="LRT")
    separator.separate_to_file(in_file, out_dir.name)
    return out_dir
