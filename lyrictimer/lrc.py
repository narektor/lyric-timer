import math
from .srt import format_timestamp as format_srt_timestamp
from .__init__ import VERSION

def to_word_lrc(lt):
    lrc = "[re:Lyric Timer - gitlab.com/narektor/lyric-timer]\n"
    lrc += f"[ve:{VERSION}]\n\n"
    # for each segment:
    for segment in lt:
        # add the lyric time
        lrc += f"[{format_timestamp(segment['from'])}]"
        # add the words
        for word in segment["words"]:
            lrc += f" <{format_timestamp(word['from'])}> {word['word'].strip()}"
        lrc += "\n"
    # return the data stripped
    return lrc.rstrip()

def to_lrc(lt):
    lrc = "[re:Lyric Timer - gitlab.com/narektor/lyric-timer]\n"
    lrc += f"[ve:{VERSION}]\n\n"
    # for each segment:
    for segment in lt:
        # add the lyric
        lrc += f"[{format_timestamp(segment['from'])}] {segment['text']}\n"
    # return the data stripped
    return lrc.rstrip()

def format_timestamp(seconds: float):
    return format_srt_timestamp(seconds, False).replace(",", ".")
