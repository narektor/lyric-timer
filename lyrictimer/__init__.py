import whisper

VERSION = "0.3"

def transcribe(
    lyrics, songfile, modelname = "small", verbose = False,
    slashes = True, language = None, split_words = False, condition_on_previous_text=True
):
    # set up whisper
    model = whisper.load_model(modelname)
    # process lyrics
    prompt = lyrics.replace("\r","")
    if slashes:
        prompt = lyrics.replace("\n", " / ")
    # transcribe
    result = model.transcribe(songfile,
                              initial_prompt=prompt, word_timestamps=split_words,
                              verbose=verbose, language=language, condition_on_previous_text=condition_on_previous_text)
    if verbose:
        print("raw result:")
        print(result)
    return simplify(result, split_words, verbose)

def simplify(result, words, verbose = False):
    segments = []
    # for each original segment:
    for w_segment in result["segments"]:
        # skip empty lyrics
        if w_segment['text'] == "":
            if w_segment['no_speech_prob'] < 0.8:
                print(f"WARNING: returned empty text at {w_segment['start']}s-{w_segment['end']}s, but silence probability is low - this indicates that an "+
                      "error has occured during transcription. Skipping.")
            if verbose:
                print(f"skipping empty lyric at {w_segment['start']}s-{w_segment['end']}s")
            continue
        # make a simplified one
        l_segment = {
            "text": w_segment['text'].strip(),
            "from": w_segment['start'],
            "to": w_segment['end']
        }
        # expose the words if requested and available
        if words and "words" in w_segment:
            l_words = []
            # simplify the word structure
            for w_word in w_segment["words"]:
                l_words.append({
                    "word": w_word["word"],
                    "from": w_word['start'],
                    "to": w_word['end']
                })
            # add the words to the segment
            l_segment["words"] = l_words
        # add it to the array
        segments.append(l_segment)
    return segments
