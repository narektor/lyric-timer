import phonemizer as ph
from thefuzz import process as fs

MAX_ACCEPTABLE_LENGTH_RATIO = 1.5

def correct_result(result, real, verbose = False):
    if verbose:
        print("Converting real lyrics to phonemes...")
    real_gr = real.split("\n")
    real_ph = []
    for lyr in real_gr:
        real_ph.append(ph.phonemize(lyr))

    new_result = []

    for lyric in result:
        correction = correct_one(lyric["text"], (real_gr, real_ph), verbose)
        # if the result is several lyrics:
        if type(correction) == list:
            print("Adding timestamps...")
            last_from = lyric["from"]
            for correct_lyr in correction:
                # find the last word of the lyric
                cl_split = correct_lyr.split(" ")
                cl_end = cl_split[-1]
                if verbose:
                    print(f"last word = {cl_end}")
                # if it was transcribed correctly:
                find = find_word(cl_end, lyric["words"], float(last_from))
                if find != None:
                    if verbose:
                        print(f"lyric ends at {find['to']}!")
                    # use the end time of the word - that's when the lyric ends
                    new_result.append({
                        "text": correct_lyr,
                        "from": last_from,
                        "to": find["to"]
                    })
                    # save the time when the lyric ends
                    last_from = find["to"]

        # if the result is one lyric, add the correction
        else:
            new_result.append({
                "text": correction,
                "from": lyric["from"],
                "to": lyric["to"]
            })

    return new_result

def find_word(word, all_words, start_from):
    # clean the word
    real_word = word.lower().strip(",.?!\"'() -")
    # for every detected word:
    for w in all_words:
        # clean the detected word
        this_word = w["word"].lower().strip(",.?!\"'() -")
        # if the word matches and it's not too early in the text,
        # it's the requested word
        if this_word == real_word and float(w["from"]) > start_from:
            return w
    return None

def correct_one(lyric, real, verbose = False, lyric_is_phonemes = False):
    # split real tuple
    real_gr = real[0]
    real_ph = real[1]

    # phonemize the lyric if necessary
    trans_ph = lyric
    if not lyric_is_phonemes:
        if verbose:
            print("Converting transcribed lyric to phonemes...")
        trans_ph = ph.phonemize(lyric)
    if verbose:
        print(real_ph)
        print("====\noriginal lyric: " + trans_ph)

    # find the real lyric using fuzzy search
    variants = fs.extract(trans_ph,real_ph,limit=2)
    if verbose:
        print("variants: ")
        print(variants)
    variant_ph = variants[0][0]
    # find the equivalent lyric in graphemes
    variant_gr = real_gr[real_ph.index(variant_ph)]
    if verbose:
        print("best variant (phonemes): " + variant_ph)
        print("best variant (graphemes): " + variant_gr)

    # detect and correct multiple lyrics being together
    if len(trans_ph) / len(variant_ph) > MAX_ACCEPTABLE_LENGTH_RATIO:
        print("Best variant is much longer than the original; the AI probably joined several lyrics together.")
        result = [variant_gr]

        # remove the suspected real lyric from the transcribed lyric
        cut_ph = trans_ph[len(variant_ph):]
        if verbose:
            print("Cut lyric: " + cut_ph)

        # correct the shortened lyric
        print("Removed current lyric, correcting the rest...")
        correct_cut = correct_one(cut_ph, real, verbose, True)
        # if the lyric itself consists of multiple lyrics (i.e. a list was returned),
        # merge it with our result list
        if type(correct_cut) == list:
            for v in correct_cut:
                if v not in result:
                    result.append(v)
        # else, just add the result
        else:
            if correct_cut not in result:
                result.append(correct_cut)

        # return the results
        return result
    else:
        return variant_gr
