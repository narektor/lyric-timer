import math
from .subtitles import make_word_segments

def to_word_srt(lt):
    new_segments = make_word_segments(lt)
    return to_srt(new_segments)

def to_srt(lt):
    srt = ""
    # for each segment (we need the index):
    for i in range(0, len(lt)):
        segment = lt[i]
        # add the number
        srt += str(i+1)
        # add the time
        srt += f"\n{format_timestamp(segment['from'])} --> {format_timestamp(segment['to'])}"
        # add the lyric
        srt += f"\n{segment['text']}\n\n\n"
    # return the data stripped
    return srt.rstrip()

def format_timestamp(seconds: float, hours_required = True):
    assert seconds >= 0, "non-negative timestamp expected"
    milliseconds = round(seconds * 1000.0)
    hours = milliseconds // 3_600_000
    milliseconds -= hours * 3_600_000
    minutes = milliseconds // 60_000
    milliseconds -= minutes * 60_000
    seconds = milliseconds // 1_000
    milliseconds -= seconds * 1_000
    hours_marker = f"{hours:02d}:"
    if not hours_required and hours_marker == "00:":
        hours_marker = ""
    return (
        f"{hours_marker}{minutes:02d}:{seconds:02d},{milliseconds:03d}"
    )
