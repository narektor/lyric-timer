# lyric timer CLI
# (c) 2023 narektor

import argparse
import pathlib
import json
import os

from . import transcribe
from .srt import to_srt, to_word_srt
from .vtt import to_vtt, to_word_vtt
from .lrc import to_lrc, to_word_lrc
from .phoneme_correction import correct_result as ph_correct
from .__init__ import VERSION

def setupParser():
    parser = argparse.ArgumentParser(
                        prog='lyrictimer',
                        description='Adds timing to song lyrics when given the song and the lyrics.')
    parser.add_argument('music_file', help='path to the song')           # positional argument
    parser.add_argument('lyric_file', help='path to the lyrics')           # positional argument
    parser.add_argument('-o', '--output', dest='output', action='store', default=None,
                        help="path to save the subtitles in")
    parser.add_argument('--model', dest='model', action='store', default="small",
                        help='Whisper model (default: small)')
    parser.add_argument('-l','--language', dest='language', action='store', default=None,
                        help='language of the song (default: auto detect)')
    parser.add_argument('-f','--format', dest='sub_format', choices=["srt","vtt","lrc","ltr"], default="srt",
                        help='the output lyrics format')

    ad_params = parser.add_argument_group('advanced parameters')
    ad_params.add_argument('-v','--verbose', dest='verbose', action='store_const',
                        const=True, default=False,
                        help='show verbose output')
    ad_params.add_argument('--no-slashes', dest='adv_slashes', action='store_const',
                        const=False, default=True,
                        help="don't replace new lines with slashes (line 1 / line 2) in the lyrics. WARNING: can dramatically reduce quality if set")
    ad_params.add_argument('--no-condition', dest='adv_condition', action='store_const',
                        const=False, default=True,
                        help="don't condition the model on previous text. WARNING: can dramatically reduce quality if set")
    ex_params = parser.add_argument_group('experimental', "These features are in development and might be unstable.")
    ex_params.add_argument('-w','--highlight-words', dest='highlight', action='store_const',
                        const=True, default=False,
                        help='highlight individual words in the subtitles')
    ex_params.add_argument('--correct', dest='phoneme_corrector', action='store_const',
                        const=True, default=False,
                        help="correct transcribed lyrics based on phonemes")
    ex_params.add_argument('-s','--split-vocals', dest='clean_vocals', action='store_const',
                        const=True, default=False,
                        help="extract the vocals and use only them for text recognition (requires spleeter)")

    dp_params = parser.add_argument_group('deprecated')
    dp_params.add_argument('--vtt', dest='vtt', action='store_const',
                        const=True, default=False,
                        help='export WebVTT (.vtt) subtitles instead of SubRip (.srt), useful for YouTube')
    return parser

def subtitle_file(result, args):
    if args.sub_format == "vtt" or args.vtt:
        if args.highlight:
            return to_word_vtt(result)
        else:
            return to_vtt(result)
    elif args.sub_format == "lrc":
        if args.highlight:
            return to_word_lrc(result)
        else:
            return to_lrc(result)
    elif args.sub_format == "ltr":
        prompt = ""
        with open(args.lyric_file, "r", encoding="utf-8") as file:
            prompt = file.read()
        dump = {
            "ver":VERSION,
            "params": {
                "pbc": args.phoneme_corrector,
                "highlight": args.highlight,
                "demix": args.clean_vocals,
                "adv":{"slashes":args.adv_slashes,"condition":args.adv_condition},
                "model":args.model,
                "lang":args.language
            },
            "result":result,
            "lyrics":prompt
        }
        return json.dumps(dump)
    else:
        if args.highlight:
            return to_word_srt(result)
        else:
            return to_srt(result)

def cli():
    # set up parser
    parser = setupParser()
    args = parser.parse_args()

    if args.vtt:
        print("\nWARNING: --vtt is deprecated and will be removed in the next version, please use --format vtt / -f vtt\n")

    if args.highlight:
        print("\nWARNING: highlighting words is an experimental feature\n")

    if args.phoneme_corrector:
        if args.highlight:
            print("ERROR: phoneme-based correction is incompatible with highlighting words")
            exit()
        print("\nWARNING: phoneme-based correction is an experimental feature\n")

    song = args.music_file
    voc_out_dir = None
    if args.clean_vocals:
        print("Loading spleeter...")
        from .demix import split_vocals
        print("Separating vocals...")
        voc_out_dir = split_vocals(song)
        if args.verbose:
            print(f"output - {voc_out_dir.name}")
        song = os.path.join(voc_out_dir.name,pathlib.Path(song).stem,"vocals.wav")

    print(f"Transcribing {song}...")
    # read lyrics
    prompt = ""
    with open(args.lyric_file, "r", encoding="utf-8") as file:
        prompt = file.read()

    # transcribe
    result = transcribe(prompt, song,
        args.model, args.verbose, split_words=args.highlight or args.phoneme_corrector,
        slashes=args.adv_slashes, condition_on_previous_text=args.adv_condition, language=args.language)
    raw_result = result

    print("Transcribed.")
    # delete split vocal folder
    if args.clean_vocals:
        voc_out_dir.cleanup()

    if args.verbose:
        print(result)

    if args.phoneme_corrector:
        print("Correcting the transcribed lyrics based on phonemes...")
        result = ph_correct(result, prompt, args.verbose)

    # get subtitle file path
    sub_path = None
    if args.output != None:
        sub_path = args.output
    elif args.sub_format == "vtt" or args.vtt:
        sub_path = pathlib.Path(args.lyric_file).stem + ".vtt"
    elif args.sub_format == "lrc":
        sub_path = pathlib.Path(args.lyric_file).stem + ".lrc"
    elif args.sub_format == "ltr":
        sub_path = pathlib.Path(args.lyric_file).stem + ".ltr"
        if args.phoneme_corrector:
            result = {"raw":raw_result,"phc":result}
    else:
        sub_path = pathlib.Path(args.lyric_file).stem + ".srt"

    # write subtitles
    print(f"Writing {sub_path}...")
    with open(sub_path, "w", encoding="utf-8") as file:
        file.write(subtitle_file(result, args))

    print(f"Done! File saved to {sub_path}")

if __name__ == "__main__":
    cli()
