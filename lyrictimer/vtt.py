import math
from .srt import format_timestamp as format_srt_timestamp
from .subtitles import make_word_segments

def to_word_vtt(lt):
    new_segments = make_word_segments(lt)
    return to_vtt(new_segments)

def to_vtt(lt):
    vtt = "WEBVTT\n\n"
    # for each segment (we need the index):
    for i in range(0, len(lt)):
        segment = lt[i]
        # add the number
        vtt += str(i+1)
        # add the time
        vtt += f"\n{format_timestamp(segment['from'])} --> {format_timestamp(segment['to'])}"
        # add the lyric
        vtt += f"\n{segment['text']}\n\n\n"
    # return the data stripped
    return vtt.rstrip()

def format_timestamp(seconds: float):
    return format_srt_timestamp(seconds).replace(",", ".")
