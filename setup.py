#!/usr/bin/env python

from distutils.core import setup
import setuptools
import pkg_resources
import os

setup(
      name='lyrictimer',
      version='0.3',
      description='Adds timing to song lyrics when given the song and the lyrics using OpenAI Whisper.',
      long_description=open("README.md", encoding="utf-8").read(),
      long_description_content_type="text/markdown",
      readme="README.md",
      python_requires=">=3.8",
      author='narektor',
	author_email='narek@torosyan.dev',
      url='https://gitlab.com/narektor/lyric-timer',
      license="LGPLv2.1",
      packages=['lyrictimer'],
      install_requires=[
            str(r)
            for r in pkg_resources.parse_requirements(
                  open(os.path.join(os.path.dirname(__file__), "requirements.txt"))
            )
      ],
      entry_points={
            "console_scripts": ["lyrictimer=lyrictimer.__main__:cli"],
      },
)
